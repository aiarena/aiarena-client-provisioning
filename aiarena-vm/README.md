Role Name
=========

This script is used to create a master image on the virtualized environment.

Requirements
------------

None

Role Variables
--------------

```
  sc2downloadurl: http://blzdistsc2-a.akamaihd.net/Linux/SC2.4.7.1.zip
  mappackurl: http://blzdistsc2-a.akamaihd.net/MapPacks/Ladder2019Season1.zip

  proxmox_node: m1nd
  proxmox_masternodeid: 500
  proxmox_aiarenanodeid: 600
  proxmox_api: 127.0.0.1
  proxmox_user: ansible@pve
  proxmox_password: <ansible-vault-encrypted> # The proxmox API password
      
  aiarena_password: <ansible-vault-encrypted> # The aiarena user password for the aiarenaclient vm
  
  root_password: <ansible-vault-encrypted>    # The root user password for the aiarenaclient vm
```

Dependencies
------------

None

Example Playbook
----------------

```
---

- name: Run aiarena-vm Playbook
  hosts: master
  gather_facts: no
  become_method: su
  become_user: root
  roles:
    - aiarena-vm
```

License
-------

BSD

Author Information
------------------

m1nd (m1nd<at>m1nd.io)
