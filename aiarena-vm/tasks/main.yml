---
# tasks file for aiarena-vm

- name: Create arenaclient-master VM
  proxmox_kvm:
    api_user    : "{{ proxmox_user }}"
    api_password: "{{ proxmox_password }}"
    api_host    : "{{ proxmox_api }}"
    name        : aiarena-client-master
    vmid        : "{{ proxmox_masternodeid }}"
    node        : "{{ proxmox_node }}"
    net         : '{"net0":"e1000=00:21:29:a1:c3:a0,bridge=vmbr0","net1":"e1000=00:21:29:a1:c3:a1,bridge=vmbr1"}'
    memory      : 8192
    cores       : 8
    cpu         : host
    storage     : vmstorage
    format      : raw
    onboot      : no
    virtio      : '{"virtio0":"vmstorage:30,cache=writeback"}'
    agent       : yes
    keyboard    : de
    kvm         : yes
    localtime   : true
    state       : present
    vga         : std
  delegate_to: node01

- name: Wait 10 seconds...
  pause:
    seconds: 10

- name: Start arenaclient-master VM
  proxmox_kvm:
    api_user    : "{{ proxmox_user }}"
    api_password: "{{ proxmox_password }}"
    api_host    : "{{ proxmox_api }}"
    name        : aiarena-client-master
    node        : "{{ proxmox_node }}"
    state       : started
  delegate_to: node01

- name: Remove old master ssh key
  shell: "ssh-keygen -f /root/.ssh/known_hosts -R 192.168.0.99"
  become: true
  delegate_to: node01

- name: Wait for VM
  wait_for:
    port: 22
    host: '{{ (ansible_ssh_host|default(ansible_host))|default(inventory_hostname) }}'
    search_regex: OpenSSH
    timeout: 6000
    delay: 5
  connection: local
  delegate_to: node01

- name: Gather facts
  setup:
  delegate_to: node01

- name: Reconfigure linux interface file from template
  template: src=interfaces.j2 dest=/etc/network/interfaces mode=0644 owner=root group=root
  become: true

- name: Restart Network
  shell: /etc/init.d/networking restart && ifup eth0 && ifup eth1
  become: true

- name: Wait 5 seconds
  pause:
    seconds: 5
  delegate_to: node01

- name: Add the aiarenawebsite IP to the system hosts file
  lineinfile:
    path: /etc/hosts
    insertafter: 'ip6-allrouters'
    line: '192.168.1.80 ai-arena.net aiarena.net dl.aiarena.net dl.ai-arena.net jenkins.m1nd.io sonar.m1nd.io'
  become: true

- name: Create install directory
  file:
    path: /home/aiarena/install/
    state: directory
    owner: aiarena
    group: aiarena

- name: Install Software via APT
  apt:
    name: "{{ packages }}"
    update_cache: yes
  vars:
    packages:
    - unzip
    - apt-transport-https
    - wget
    - dirmngr
    - git
    - sudo
    - curl
    - ntp
    - isc-dhcp-client
    - build-essential
    - autoconf
    - automake1.11
    - libtool
    - flex
    - bison
    - debhelper
    - binutils-gold
    - python
  become: true

- name: Download and extract JailKit
  unarchive:
    src: https://olivier.sessink.nl/jailkit/jailkit-2.21.tar.gz
    dest: /root/jailkit
    remote_src: yes
    owner: root
    group: root
  become: true

- name: Create JailKit package and install it
  shell: cd /root/jailkit && echo 5 > debian/compat && ./debian/rules binary && cd .. && dpkg -i jailkit_2.20-1_amd64.deb && rm -Rf jailkit*
  become: true

- name: Update the JailKit template file
  template:
    src: jk_init.ini.j2
    dest: /etc/jailkit/jk_init.ini
    owner: root
    group: root
    mode: '0644'
  become: true

- name: Add the botuser_a group
  group:
    name: botuser_a
    state: present
  become: true

- name: Add the botuser_b group
  group:
    name: botuser_b
    state: present
  become: true

- name: Add the botuser_a user
  user:
    name: botuser_a
    group: botuser_a
  become: true

- name: Add the botuser_b user
  user:
    name: botuser_b
    group: botuser_b
  become: true

- name: Create chroot jail directory for botuser_a
  shell: mkdir -p /botuser_a_jail
  become: true

- name: Create chroot jail directory for botuser_b
  shell: mkdir -p /botuser_b_jail
  become: true

- name: Remove ntpdate
  apt:
    name: ntpdate
    update_cache: yes
    state: absent
  become: true
  
- name: Setup NTP config from template
  template: src=ntp.conf.j2 dest=/etc/ntp.conf mode=0644 owner=root group=root
  become: true

- name: DotNetCore - Download Microsoft Repo package
  shell: "wget http://dl.ai-arena.net/packages-microsoft-prod.deb -O /tmp/packages-microsoft-prod.deb"
  become: true

- name: DotNetCore - Install Microsoft Repo package
  shell: "dpkg -i /tmp/packages-microsoft-prod.deb"
  become: true

- name: Update APT cache and accept release info change
  shell: "apt-get update --allow-releaseinfo-change"
  become: true

- name: DotNetCore - Install dotnet-sdk via APT
  apt:
    name: dotnet-sdk-2.2
  become: true

- name: Wine - Add i386 arch
  shell: "dpkg --add-architecture i386"
  become: true

- name: Wine - Install SUSE Repo Key
  apt_key:
    url: https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/Debian_10/Release.key
    state: present
  become: true

- name: Wine - Add Wine Repository
  apt_repository:
    repo: deb https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/Debian_10 ./
    state: present
    filename: winesuse
  become: true

- name: Wine - Install Wine Repo Key
  apt_key:
    url: https://dl.winehq.org/wine-builds/winehq.key
    state: present
  become: true

- name: Wine - Add Wine Repository
  apt_repository:
    repo: deb https://dl.winehq.org/wine-builds/debian/ buster main
    state: present
    filename: wine
  become: true

- name: Wine - Install winehq-stable via APT
  apt:
    name: "{{ packages }}"
    update_cache: yes
    state: latest
    install_recommends: yes
  vars:
    packages:
    - winehq-stable
    - wine-stable
    - wine-stable-i386
  become: true

- name: Java - Install Zulu Repo Key
  shell: apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0xB1998361219BD9C9
  become: true

- name: Java - Add zulu repo for openjdk-12
  apt_repository:
    repo: deb http://repos.azulsystems.com/debian stable main
    state: present
    filename: zulu
  become: true

- name: Java - Install OpenJDK 12 via APT
  apt:
    name: zulu-12
    update_cache: yes
  become: true

- name: Python - Install python3.7
  apt:
    name: "{{ packages }}"
    update_cache: yes
  vars:
    packages:
    - python3
    - python3-venv
    - python-dev
    - python3-dev
    - python3-pip
    - python-pip
    - build-essential
    - make
    - python3-igraph
  become: true

- name: Python - Install setuptools globally
  shell: pip install setuptools
  become: true

- name: Python - Create requirements.txt from template
  template: src=python-requirements.txt.j2 dest=/home/aiarena/install/requirements.txt mode=0644 owner=aiarena group=aiarena

- name: Python - Instal required packages globally
  shell: pip3 install -r /home/aiarena/install/requirements.txt
  become: true

- name: Download aiarena-client python requirements
  shell: wget https://gitlab.com/aiarena/aiarena-client/-/raw/master/requirements.txt -O /tmp/aiarena-client-python.txt

- name: Python - Instal aiarena client required packages globally
  shell: pip3 install -r /tmp/aiarena-client-python.txt
  become: true

- name: Download and extract SC2
  unarchive:
    src: "{{ sc2downloadurl }}"
    dest: /home/aiarena/
    remote_src: yes
    extra_opts:
      - '-P'
      - 'iagreetotheeula'

- name: Create maps directory symlink
  file:
    src: "/home/aiarena/StarCraftII/Maps"
    dest: "/home/aiarena/StarCraftII/maps"
    state: link

- name: Remove preinstalled SC2 Maps
  shell: rm -Rf /home/aiarena/StarCraftII/Maps/*

- name: Download and extract SC2 Maps
  unarchive:
    src: "{{ mappackurl }}"
    dest: /home/aiarena/StarCraftII/Maps/
    remote_src: yes

- name: Create bashrc for aiarena User
  template: src=bashrc.j2 dest=/home/aiarena/.bashrc mode=0644 owner=aiarena group=aiarena

- name: Removing possibly existing ArenaClient directory
  file:
    path: /home/aiarena/aiarena-client
    state: absent
  become: true

- name: Download and extract ArenaClient
  unarchive:
    src: http://dl.aiarena.net/aiarena-client-master.tar.gz
    dest: /home/aiarena/
    remote_src: yes
    owner: aiarena
    group: aiarena

- name: Install the aiarena client module
  shell: cd /home/aiarena/aiarena-client-master && pip3 install .
  become: true

- name: Removing ArenaClient install directory
  file:
    path: /home/aiarena/aiarena-client-master
    state: absent

- name: Create ArenaClient directory
  file:
    path: /home/aiarena/aiarena-client
    state: directory
    owner: aiarena
    group: aiarena

- name: Create Replay directory
  file:
    path: /home/aiarena/aiarena-client/replays
    state: directory
    owner: aiarena
    group: aiarena

- name: Init the chroot jail for botuser_a
  shell: jk_init -v /botuser_a_jail netbasics uidbasics jk_lsh basicshell python3 wine dotnet java
  become: true

- name: Init the chroot jail for botuser_b
  shell: jk_init -v /botuser_b_jail netbasics uidbasics jk_lsh basicshell python3 wine dotnet java
  become: true

- name: Assign botuser_a to the jail
  shell: jk_jailuser -m -j /botuser_a_jail botuser_a
  become: true

- name: Assign botuser_b to the jail
  shell: jk_jailuser -m -j /botuser_b_jail botuser_b
  become: true

- name: Stop arenaclient-master VM
  proxmox_kvm:
    api_user    : "{{ proxmox_user }}"
    api_password: "{{ proxmox_password }}"
    api_host    : "{{ proxmox_api }}"
    name        : aiarena-client-master
    node        : "{{ proxmox_node }}"
    state       : stopped
  delegate_to: node01

- name: Remove internal NIC
  shell: pvesh set /nodes/"{{ proxmox_node }}"/qemu/"{{ proxmox_masternodeid }}"/config --delete net1
  delegate_to: node01

- name: Reconfigure arenaclient-master VM
  proxmox_kvm:
    api_user    : "{{ proxmox_user }}"
    api_password: "{{ proxmox_password }}"
    api_host    : "{{ proxmox_api }}"
    name        : aiarena-client-master
    node        : "{{ proxmox_node }}"
    cores       : 2
    cpu         : host
    memory      : 6144
    update      : yes
  delegate_to: node01

- name: Remove Master VM from Proxmox Cluster - vmstorage-storage
  proxmox_kvm:
    api_user    : "{{ proxmox_user }}"
    api_password: "{{ proxmox_password }}"
    api_host    : "{{ proxmox_api }}"
    name        : aiarena-client
    node        : "{{ proxmox_node }}"
    state       : absent
  delegate_to: node01

- name: Clone VM
  shell: qm clone 500 600 -name aiarena-client -target node01 --storage vmstorage -full 1
  delegate_to: node01

- name: Remove Master VM from Proxmox Cluster
  proxmox_kvm:
    api_user    : "{{ proxmox_user }}"
    api_password: "{{ proxmox_password }}"
    api_host    : "{{ proxmox_api }}"
    name        : aiarena-client-master
    node        : "{{ proxmox_node }}"
    state       : absent
  delegate_to: node01

- name: Wait 5 seconds
  pause:
    seconds: 5
  delegate_to: node01

- name: Add internal NIC
  shell: pvesh set /nodes/"{{ proxmox_node }}"/qemu/"{{ proxmox_aiarenanodeid }}"/config --net1 'virtio=00:21:29:a1:c3:a2,bridge=vmbr1'
  delegate_to: node01

- name: Start arenaclient-"{{ proxmox_aiarenanodeid }}"
  shell: qm start {{ proxmox_aiarenanodeid }}
  delegate_to: node01

- name: Remove old master ssh key
  shell: "ssh-keygen -f /root/.ssh/known_hosts -R 192.168.1.100"
  become: true
  delegate_to: node01

- name: Wait for VM"{{ proxmox_aiarenanodeid }}" SSH
  wait_for:
    port: 22
    host: '192.168.1.100'
    search_regex: OpenSSH
    timeout: 6000
    delay: 5
  connection: local

- name: Wait 5 seconds
  pause:
    seconds: 5
  delegate_to: node01

- name: Deploy local root ssh key
  authorized_key:
    user: root
    state: present
    key: "{{ lookup('file', '/root/.ssh/id_rsa.pub') }}"
  delegate_to: arenaclient
  become: true

- name: Allow root SSH access
  lineinfile: dest=/etc/ssh/sshd_config
              regexp="^PermitRootLogin"
              line="PermitRootLogin yes"
              state=present
  delegate_to: arenaclient
  become: true

- name: Disable IPV6
  shell: 'echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf && sysctl -p'
  delegate_to: arenaclient
  become: true

- name: Change aiarena password
  user: name=aiarena password={{ aiarena_password | string | password_hash('sha512') }} update_password=always
  become: true
  delegate_to: arenaclient

- name: Change root password
  user: name=root password={{ root_password | string | password_hash('sha512') }} update_password=always
  become: true
  delegate_to: arenaclient

- name: Stop arenaclient-"{{ proxmox_aiarenanodeid }}"
  shell: qm stop {{ proxmox_aiarenanodeid }}
  delegate_to: node01

- name: Remove internet NIC
  shell: pvesh set /nodes/"{{ proxmox_node }}"/qemu/"{{ proxmox_aiarenanodeid }}"/config --delete net0
  delegate_to: node01
