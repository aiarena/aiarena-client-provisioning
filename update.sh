# Download Arena Client
wget https://gitlab.com/aiarena/aiarena-client/-/archive/master/aiarena-client-master.tar.gz

# Transfer to Webserver
scp aiarena-client-master.tar.gz root@192.168.0.80:/var/www/dl.ai-arena.net/htdocs/aiarena-client-master.tar.gz

# Delete client copy
rm aiarena-client-master.tar.gz

ansible-playbook -i hosts --diff aiarena-client-configure.yml
sleep 10
ansible-playbook -i hosts --diff aiarena-snapshot-clones.yml
