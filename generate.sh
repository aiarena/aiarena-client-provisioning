#!/bin/sh

# Download Arena Client
wget https://gitlab.com/aiarena/aiarena-client/-/archive/master/aiarena-client-master.tar.gz
# Transfer to Webserver
scp aiarena-client-master.tar.gz root@192.168.0.80:/var/www/dl.ai-arena.net/htdocs/aiarena-client-master.tar.gz
# Delete client copy
rm aiarena-client-master.tar.gz

# Download MS APT repo package
wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb
# Transfer MS APT repo package to Webserver
scp packages-microsoft-prod.deb root@192.168.0.80:/var/www/dl.ai-arena.net/htdocs/packages-microsoft-prod.deb

# Delete MS APT repo package
rm packages-microsoft-prod.deb

ansible-playbook -i hosts --diff aiarena-vm.yml
sleep 5
ansible-playbook -i hosts --diff aiarena-delete-clones.yml
sleep 5
ansible-playbook -i hosts --diff aiarena-clones.yml
sleep 10
ansible-playbook -i hosts --diff aiarena-client-configure.yml
sleep 10
ansible-playbook -i hosts --diff aiarena-snapshot-clones.yml
